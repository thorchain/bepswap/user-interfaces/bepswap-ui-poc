BEPSwap
=============

UI (PoC) for [BEPSwap](https://thorchain.com)

## Setup
```bash
yarn install
```

## Start
```bash
yarn start
```

## Deploy
```bash
yarn deploy
```

To deploy production...
```bash
ENVIRONMENT=prod yarn deploy
```
