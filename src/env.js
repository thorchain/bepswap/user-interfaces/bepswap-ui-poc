const isTestnet = true
const isMainnet = !isTestnet
const poolAddress = "tbnb10sfan8kwwmvx0ly89q3tc0zacjhznvurjdh664"

const NET = isTestnet ? "testnet" : "mainnet"

export {
  NET,
  isTestnet,
  isMainnet,
  poolAddress
}
